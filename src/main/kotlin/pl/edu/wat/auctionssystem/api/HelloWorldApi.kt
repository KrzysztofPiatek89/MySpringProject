package pl.edu.wat.auctionssystem.api

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/hello")
class HelloWorldApi {

    @GetMapping("/world")
    fun sayHello(): String {
        return "Witaj w aplikacji Actions System"
    }

}