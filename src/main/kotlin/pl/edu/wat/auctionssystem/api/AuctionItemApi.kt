package pl.edu.wat.auctionssystem.api

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pl.edu.wat.auctionssystem.model.AuctionItem
import pl.edu.wat.auctionssystem.model.ItemsRequest
import pl.edu.wat.auctionssystem.service.AuctionItemService

@RestController
@RequestMapping("/auction")
class AuctionItemApi(
    val auctionItemService: AuctionItemService
) {

    @GetMapping("/item")
    fun addAuctionItem(@RequestParam name: String, price: Double): AuctionItem {
        return auctionItemService.addAuctionItem(name,price)
    }
    @GetMapping("/{id}")
    @ResponseBody
    fun getAuctionItem(@PathVariable id: Long):AuctionItem {
        return auctionItemService.getAuctionItem(id)
    }

    @GetMapping("/all")
    fun getAllItems(): List<AuctionItem> {
        return auctionItemService.getAllItems()
    }
    @GetMapping("/get/{userId}")
    @ResponseBody
    fun getUserById(@PathVariable userId: Long): AuctionItem {
        return auctionItemService.getAuctionItem(userId)
    }

    @PutMapping("/put/{userId}")
    @ResponseBody
    fun updateAuctionItem(@PathVariable("userId") idItem: Long,@RequestBody auctionItemsRequest: ItemsRequest): ResponseEntity<ItemsRequest> {


        try {
            val newItem = auctionItemService.uploadAuctionItem(auctionItemsRequest, idItem)
            return ResponseEntity(newItem, HttpStatus.OK)
        } catch (ex: IllegalAccessException) {
            return ResponseEntity(HttpStatus.UNAUTHORIZED)
        } catch (e: Exception){
            return ResponseEntity(HttpStatus.BAD_REQUEST)
        }        }








}


