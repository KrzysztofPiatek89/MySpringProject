package pl.edu.wat.auctionssystem.api

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import pl.edu.wat.auctionssystem.model.MerchantInfo
import pl.edu.wat.auctionssystem.service.MerchantInfoService

@RestController
@RequestMapping("/merchant")
class MerchantInfoApi(
        val merchantInfoService: MerchantInfoService
) {

    @GetMapping("/info")
    fun addTestMerchant(@RequestParam (value = "name") shopName: String,@RequestParam comment: String): MerchantInfo{
        return merchantInfoService.addTestMerchantInfo(shopName,comment)
    }
    @GetMapping("/all-merchants")
    fun getAllMerchantInfo(): List<MerchantInfo> {
        return merchantInfoService.getAllMerchantInfo()
    }
}