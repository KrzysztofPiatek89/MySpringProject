package pl.edu.wat.auctionssystem.api

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pl.edu.wat.auctionssystem.model.AUser
import pl.edu.wat.auctionssystem.model.AUserRequest
import pl.edu.wat.auctionssystem.service.AUserService
import java.util.*

@RestController
@RequestMapping("/user")
class AUserApi(
    val aUserService: AUserService
) {

    @GetMapping("/test")
    fun addTestUser(): AUser {
        return aUserService.addTestUser()
    }

    @GetMapping("/all")
    fun getAll(): List<AUser> {
        return aUserService.getAllUsers()
    }

    @GetMapping("/get/{userId}")
    @ResponseBody
    fun getUserById(@PathVariable userId: Long): Optional<AUser> {
        return aUserService.getUser(userId)
    }

    @PutMapping("/put/{userId}")
    @ResponseBody
    fun updateUser(@PathVariable("userId") id: Long, @RequestBody userReq: AUserRequest): ResponseEntity<AUserRequest> {
        try {
            val newUser = aUserService.uploadUser(userReq,id)
            return ResponseEntity(newUser, HttpStatus.CREATED)
        } catch (ex: IllegalAccessException) {
            return ResponseEntity(HttpStatus.UNAUTHORIZED)
        } catch (e: Exception) {
            return ResponseEntity(HttpStatus.BAD_REQUEST)
        }
    }
}