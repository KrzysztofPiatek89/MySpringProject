package pl.edu.wat.auctionssystem.model

import com.fasterxml.jackson.annotation.JsonBackReference
import javax.persistence.*

@Entity
data class CustomerInfo(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,
    val email: String,
    val shippingAddress: String? = null,
    val phone: String? = null
) {

    @JsonBackReference
    @OneToOne(mappedBy = "customerInfo")
    val user: AUser? = null

}
