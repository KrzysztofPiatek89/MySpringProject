package pl.edu.wat.auctionssystem.model

import com.fasterxml.jackson.annotation.JsonBackReference
import java.time.LocalDateTime
import java.util.*
import java.util.spi.CalendarDataProvider
import javax.persistence.*

@Entity
data class AuctionItem(

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    val id: Long? = null,
    val name: String,
    val description: String,
    val sellcost: String,
    //  val startdate: LocalDateTime,
    //  val completionData: Date,
    val price: Double,
) {
    @ManyToOne
    @JoinColumn(name="user_id")
    @JsonBackReference
    val user: AUser? = null
}



