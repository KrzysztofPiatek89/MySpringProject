package pl.edu.wat.auctionssystem.model

import com.fasterxml.jackson.annotation.JsonManagedReference
import javax.persistence.*

@Entity
data class AUser(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,
    val username: String,
    val password: String,
    val nationality: String,
    val isBanned: Boolean = false,

    @JsonManagedReference
    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "customer_info_id")
    val customerInfo: CustomerInfo,

    @JsonManagedReference
    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "merchant_info_id")
    val merchantInfo: MerchantInfo,

    @JsonManagedReference
    @OneToMany(cascade = [CascadeType.ALL], mappedBy = "user")
    val auctions: List<AuctionItem>
    )
