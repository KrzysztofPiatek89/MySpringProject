package pl.edu.wat.auctionssystem.model

import pl.edu.wat.auctionssystem.repository.AUserRepository

data class AUserRequest(
    val username: String,
    val password: String,
    val nationality: String
)