package pl.edu.wat.auctionssystem.model

import com.fasterxml.jackson.annotation.JsonBackReference
import javax.persistence.*


@Entity
data class MerchantInfo (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,
    val plus: Int,
    val minus: Int,
    val shopName: String,
    val comment: String,
    val phone: String,
    val ratingOfCompatibility: Int,
    val ratingOfQuality: Int
) {
    @OneToOne(mappedBy = "merchantInfo")
    @JsonBackReference
    val user: AUser?=null
}