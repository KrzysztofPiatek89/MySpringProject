package pl.edu.wat.auctionssystem.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import pl.edu.wat.auctionssystem.model.AUser

@Repository
interface AUserRepository : JpaRepository<AUser, Long>

