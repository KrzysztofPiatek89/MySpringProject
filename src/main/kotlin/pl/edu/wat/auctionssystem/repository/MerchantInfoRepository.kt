package pl.edu.wat.auctionssystem.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import pl.edu.wat.auctionssystem.model.MerchantInfo

@Repository
interface MerchantInfoRepository : JpaRepository<MerchantInfo , Long>