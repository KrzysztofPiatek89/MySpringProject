package pl.edu.wat.auctionssystem.service

import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.RequestParam
import pl.edu.wat.auctionssystem.api.MerchantInfoApi
import pl.edu.wat.auctionssystem.model.MerchantInfo
import pl.edu.wat.auctionssystem.repository.MerchantInfoRepository
import javax.xml.stream.events.Comment
import kotlin.random.Random


@Service
class MerchantInfoService (
    val merchantInfoRepository: MerchantInfoRepository,
) {
    fun addTestMerchantInfo(@RequestParam shopName: String,@RequestParam comment: String): MerchantInfo {

        val randomPhoneNumber = Random.nextInt(100000000,999999999)
        val randomComp = Random.nextInt(0,5)
        val randomQual = Random.nextInt(0,5)
        val testMerchantInfo = MerchantInfo(
                plus = 5,
                minus = 1,
                shopName = shopName,
                comment = comment,
                phone = "phone: $randomPhoneNumber",
                ratingOfCompatibility = randomComp,
                ratingOfQuality = randomQual
        )
        val merchantInfoSavedInDatabase = merchantInfoRepository.save(testMerchantInfo)
        return merchantInfoSavedInDatabase
    }

    fun getAllMerchantInfo(): List<MerchantInfo>{
        return merchantInfoRepository.findAll()
    }
}

