package pl.edu.wat.auctionssystem.service

import org.springframework.stereotype.Service
import pl.edu.wat.auctionssystem.model.*
import pl.edu.wat.auctionssystem.repository.AUserRepository
import java.util.*
import kotlin.random.Random

@Service
class AUserService(
    val aUserRepository: AUserRepository
) {

    fun addTestUser():  AUser {
        val randomNumber = Random.nextInt(0, 100)

        val testUser = AUser(
            username = "Test user $randomNumber",
            password = "1234",
            nationality = "Poland",
            customerInfo = CustomerInfo(
                email = "sample@sample.pl",
                shippingAddress = "Test addres, ul. Test 3/4, 00-000 Test"
            ),

            merchantInfo = MerchantInfo(
                plus = 1,
                minus = 1,
                shopName = "something",
                comment = "warto zobaczyc",
                phone = "999800001",
                ratingOfQuality = 2,
                ratingOfCompatibility = 4),

            auctions = listOf(
                AuctionItem(
                    name = "Kuba",
                    description = "Allegro",
                    sellcost = " 200",
                    price = 200.0
                ),
                AuctionItem(
                    name = "Kuba",
                    description = "Allegro",
                    sellcost = " 200",
                    price = 200.0
                ),
                AuctionItem(
                    name = "Kuba",
                    description = "Allegro",
                    sellcost = " 200",
                    price = 200.0
                ),
                AuctionItem(
                    name = "Kuba",
                    description = "Allegro",
                    sellcost = " 200",
                    price = 200.0
                ),
            )

        )
        val userSavedInDatabase = aUserRepository.save(testUser)
        return userSavedInDatabase
    }

    fun getAllUsers(): List<AUser> {
        return aUserRepository.findAll()
    }

    fun getUser(userId: Long): Optional<AUser> {
        return aUserRepository.findById(userId)
    }

    fun uploadUser(userRequest: AUserRequest, userId: Long): AUserRequest? {
        val aUser = aUserRepository.findById(userId)
        aUser.ifPresent {
            val userToUpdate = it.copy(
                username = userRequest.username,
                password = userRequest.password,
                nationality = userRequest.nationality
            )
            aUserRepository.save(userToUpdate)
        }
        return null
    }
}
