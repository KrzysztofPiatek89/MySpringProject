package pl.edu.wat.auctionssystem.service

import org.springframework.stereotype.Service
import pl.edu.wat.auctionssystem.model.AuctionItem
import pl.edu.wat.auctionssystem.model.ItemsRequest
import pl.edu.wat.auctionssystem.repository.AuctionItemRepository
import kotlin.random.Random

@Service
class AuctionItemService(
    val auctionItemRepository: AuctionItemRepository
) {

    fun addAuctionItem(name: String,price: Double): AuctionItem {
        val randomNumber = Random.nextInt(0, 200)

        val testUser = AuctionItem(
            name = name,
            description = "AUKCJA",
            sellcost = "$randomNumber",
          //  startdate = "",
          //  completionData = ,
            price = price
        )
        val userSavedInDatabase = auctionItemRepository.save(testUser)
        return userSavedInDatabase
    }


    fun getAllItems(): List<AuctionItem> {
        return auctionItemRepository.findAll()
    }
    fun getAuctionItem(id:Long): AuctionItem {
        return auctionItemRepository.getById(id)
    }

    fun uploadAuctionItem(itemRequest: ItemsRequest, idItem: Long ):ItemsRequest?{
        val item = auctionItemRepository.findById(idItem)
        item.ifPresent {
            val updateItem=it.copy(
                name = itemRequest.name,
                description = itemRequest.description,
                sellcost = itemRequest.sellcost,
                price = itemRequest.price
            )
            auctionItemRepository.save(updateItem)
        }
        return null
    }
}
