package pl.edu.wat.auctionssystem

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AuctionsSystemApplication

fun main(args: Array<String>) {
    runApplication<AuctionsSystemApplication>(*args)
}
 